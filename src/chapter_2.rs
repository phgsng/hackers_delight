/*! BASICS */

pub mod ch2_1_rightmost_bits
{
    /*! 2-1 Manipulating rightmost bits */

    /** The ``wrapping_sub()`` is required to avoid rust’s habit of panicking on
    unsigned overflow in debug mode … */
    pub fn off_rightmost_one_bit(x: u8) -> u8 { x & (x.wrapping_sub(1)) }

    /** Use the above to check for `∃n: x = 2^n`. Effectively this tests for
    a popcnt of one. */
    pub fn is_power_of_two_or_zero(x: u8) -> bool
    {
        off_rightmost_one_bit(x) == 0
    }

    pub fn on_rightmost_zero_bit(x: u8) -> u8 { x | (x.wrapping_add(1)) }

    pub fn off_trailing_ones(x: u8) -> u8 { x & (x.wrapping_add(1)) }

    pub fn on_trailing_zeros(x: u8) -> u8 { x | (x.wrapping_sub(1)) }

    /** Create a word with a single 1-bit at the position of the rightmost 0-bit
    in ``x``. */
    pub fn mask_rightmost_zero(x: u8) -> u8 { !x & (x.wrapping_add(1)) }

    /** Create a word with a singel 0-bit at the position of the rightmost 1-bit
    in ``x``. */
    pub fn unmask_rightmost_one(x: u8) -> u8 { !x | x.wrapping_sub(1) }

    pub fn mask_trailing_zeros_1(x: u8) -> u8 { !x & x.wrapping_sub(1) }

    /** The signed casting shenanigans are due to rustc’s refusal to negate
    unsigned values. Why can’t we just commit to two’s complement and stop the
    bs? */
    pub fn mask_trailing_zeros_2(x: u8) -> u8 { !(x | 0u8.wrapping_sub(x)) }

    pub fn mask_trailing_zeros_3(x: u8) -> u8
    {
        (x & 0u8.wrapping_sub(x)).wrapping_sub(1)
    }

    pub fn unmask_trailing_ones(x: u8) -> u8 { !x | x.wrapping_add(1) }

    pub fn isolate_rightmost_one(x: u8) -> u8 { x & 0u8.wrapping_sub(x) }

    pub fn one_rightmost_one_trailing_zero(x: u8) -> u8
    {
        x ^ (x.wrapping_sub(1))
    }

    pub fn one_rightmost_zero_trailing_one(x: u8) -> u8
    {
        x ^ (x.wrapping_add(1))
    }

    /** Can be used to test for diff of two powers of two by checking for zero
    result. */
    pub fn unset_rightmost_contiguous_ones_1(x: u8) -> u8
    {
        ((x | (x.wrapping_sub(1))).wrapping_add(1)) & x
    }

    pub fn unset_rightmost_contiguous_ones_2(x: u8) -> u8
    {
        ((x & 0u8.wrapping_sub(x)).wrapping_add(x)) & x
    }

    #[cfg(test)]
    mod validate
    {
        #[test]
        fn off_rightmost_one_bit()
        {
            use super::off_rightmost_one_bit as off_r1s;
            assert_eq!(off_r1s(0b_0000_0000), 0b0000_0000);
            assert_eq!(off_r1s(0b_0000_0001), 0b0000_0000);
            assert_eq!(off_r1s(0b_0000_0011), 0b0000_0010);
            assert_eq!(off_r1s(0b_0101_1000), 0b0101_0000);
            assert_eq!(off_r1s(0b_1000_0000), 0b0000_0000);
            assert_eq!(off_r1s(0b_1111_1111), 0b1111_1110);
        }

        #[test]
        fn is_power_of_two_or_zero()
        {
            use super::is_power_of_two_or_zero as is_p2;
            assert!(is_p2(0b_0000_0000));
            assert!(is_p2(0b_0000_0001));
            assert!(!is_p2(0b_0000_0011));
            assert!(is_p2(0b_0000_0010));
            assert!(is_p2(0b_1000_0000));
            assert!(!is_p2(0b_0101_1000));
            assert!(!is_p2(0b_1111_1111));
        }

        #[test]
        fn on_rightmost_zero_bit()
        {
            use super::on_rightmost_zero_bit as on_r0s;
            assert_eq!(on_r0s(0b_0000_0000), 0b0000_0001);
            assert_eq!(on_r0s(0b_0000_0001), 0b0000_0011);
            assert_eq!(on_r0s(0b_0000_0011), 0b0000_0111);
            assert_eq!(on_r0s(0b_0000_0010), 0b0000_0011);
            assert_eq!(on_r0s(0b_0101_1000), 0b0101_1001);
            assert_eq!(on_r0s(0b_0111_1111), 0b1111_1111);
            assert_eq!(on_r0s(0b_1110_1111), 0b1111_1111);
            assert_eq!(on_r0s(0b_1111_1110), 0b1111_1111);
            assert_eq!(on_r0s(0b_1111_1111), 0b1111_1111);
        }

        #[test]
        fn off_trailing_ones()
        {
            use super::off_trailing_ones as off_t1;
            assert_eq!(off_t1(0b_0000_0000), 0b0000_0000);
            assert_eq!(off_t1(0b_0000_0001), 0b0000_0000);
            assert_eq!(off_t1(0b_0000_0011), 0b0000_0000);
            assert_eq!(off_t1(0b_0001_0011), 0b0001_0000);
            assert_eq!(off_t1(0b_0000_0010), 0b0000_0010);
            assert_eq!(off_t1(0b_0101_1000), 0b0101_1000);
            assert_eq!(off_t1(0b_0111_1111), 0b0000_0000);
            assert_eq!(off_t1(0b_1110_1111), 0b1110_0000);
            assert_eq!(off_t1(0b_1111_1110), 0b1111_1110);
            assert_eq!(off_t1(0b_1111_1111), 0b0000_0000);
        }

        #[test]
        fn on_trailing_zeros()
        {
            use super::on_trailing_zeros as on_t0;
            assert_eq!(on_t0(0b_0000_0000), 0b1111_1111);
            assert_eq!(on_t0(0b_0000_0001), 0b0000_0001);
            assert_eq!(on_t0(0b_0000_0011), 0b0000_0011);
            assert_eq!(on_t0(0b_0001_0011), 0b0001_0011);
            assert_eq!(on_t0(0b_0000_0010), 0b0000_0011);
            assert_eq!(on_t0(0b_0101_1000), 0b0101_1111);
            assert_eq!(on_t0(0b_0111_1111), 0b0111_1111);
            assert_eq!(on_t0(0b_1110_1111), 0b1110_1111);
            assert_eq!(on_t0(0b_1111_1110), 0b1111_1111);
            assert_eq!(on_t0(0b_1111_1111), 0b1111_1111);
        }

        #[test]
        fn mask_rightmost_zero()
        {
            use super::mask_rightmost_zero as on_r0;
            assert_eq!(on_r0(0b_0000_0000), 0b_0000_0001);
            assert_eq!(on_r0(0b_0000_0001), 0b_0000_0010);
            assert_eq!(on_r0(0b_0000_0011), 0b_0000_0100);
            assert_eq!(on_r0(0b_0001_0011), 0b_0000_0100);
            assert_eq!(on_r0(0b_0000_0010), 0b_0000_0001);
            assert_eq!(on_r0(0b_0101_1000), 0b_0000_0001);
            assert_eq!(on_r0(0b_0111_1111), 0b_1000_0000);
            assert_eq!(on_r0(0b_1110_1111), 0b_0001_0000);
            assert_eq!(on_r0(0b_1111_1110), 0b_0000_0001);
            assert_eq!(on_r0(0b_1111_1111), 0b_0000_0000);
        }

        #[test]
        fn unmask_rightmost_one()
        {
            use super::unmask_rightmost_one as off_r1;
            assert_eq!(off_r1(0b_0000_0000), 0b_1111_1111);
            assert_eq!(off_r1(0b_0000_0001), 0b_1111_1110);
            assert_eq!(off_r1(0b_0000_0011), 0b_1111_1110);
            assert_eq!(off_r1(0b_0001_0011), 0b_1111_1110);
            assert_eq!(off_r1(0b_0000_0010), 0b_1111_1101);
            assert_eq!(off_r1(0b_0101_1000), 0b_1111_0111);
            assert_eq!(off_r1(0b_0111_1111), 0b_1111_1110);
            assert_eq!(off_r1(0b_1000_0000), 0b_0111_1111);
            assert_eq!(off_r1(0b_1110_1111), 0b_1111_1110);
            assert_eq!(off_r1(0b_1111_1110), 0b_1111_1101);
            assert_eq!(off_r1(0b_1111_1111), 0b_1111_1110);
        }

        #[test]
        fn mask_trailing_zeros()
        {
            let chk = |mt0: fn(u8) -> u8| {
                assert_eq!(mt0(0b_0000_0000), 0b_1111_1111);
                assert_eq!(mt0(0b_0000_0001), 0b_0000_0000);
                assert_eq!(mt0(0b_0000_0011), 0b_0000_0000);
                assert_eq!(mt0(0b_0001_0011), 0b_0000_0000);
                assert_eq!(mt0(0b_0000_0010), 0b_0000_0001);
                assert_eq!(mt0(0b_0101_1000), 0b_0000_0111);
                assert_eq!(mt0(0b_0111_1111), 0b_0000_0000);
                assert_eq!(mt0(0b_1000_0000), 0b_0111_1111);
                assert_eq!(mt0(0b_1110_1111), 0b_0000_0000);
                assert_eq!(mt0(0b_1111_1110), 0b_0000_0001);
                assert_eq!(mt0(0b_1111_1111), 0b_0000_0000);
            };

            chk(super::mask_trailing_zeros_1);
            chk(super::mask_trailing_zeros_2);
            chk(super::mask_trailing_zeros_3);
        }

        #[test]
        fn unmask_trailing_ones()
        {
            use super::unmask_trailing_ones as ut1;
            assert_eq!(ut1(0b_0000_0000), 0b_1111_1111);
            assert_eq!(ut1(0b_0000_0001), 0b_1111_1110);
            assert_eq!(ut1(0b_0000_0011), 0b_1111_1100);
            assert_eq!(ut1(0b_0001_0011), 0b_1111_1100);
            assert_eq!(ut1(0b_0000_0010), 0b_1111_1111);
            assert_eq!(ut1(0b_0101_1000), 0b_1111_1111);
            assert_eq!(ut1(0b_0111_1111), 0b_1000_0000);
            assert_eq!(ut1(0b_1000_0000), 0b_1111_1111);
            assert_eq!(ut1(0b_1110_1111), 0b_1111_0000);
            assert_eq!(ut1(0b_1111_1110), 0b_1111_1111);
            assert_eq!(ut1(0b_1111_1111), 0b_0000_0000);
        }

        #[test]
        fn isolate_rightmost_one()
        {
            use super::isolate_rightmost_one as ir1;
            assert_eq!(ir1(0b_0000_0000), 0b_0000_0000);
            assert_eq!(ir1(0b_0000_0001), 0b_0000_0001);
            assert_eq!(ir1(0b_0000_0011), 0b_0000_0001);
            assert_eq!(ir1(0b_0001_0011), 0b_0000_0001);
            assert_eq!(ir1(0b_0000_0010), 0b_0000_0010);
            assert_eq!(ir1(0b_0101_1000), 0b_0000_1000);
            assert_eq!(ir1(0b_0111_1111), 0b_0000_0001);
            assert_eq!(ir1(0b_1000_0000), 0b_1000_0000);
            assert_eq!(ir1(0b_1110_1111), 0b_0000_0001);
            assert_eq!(ir1(0b_1111_1110), 0b_0000_0010);
            assert_eq!(ir1(0b_1111_1111), 0b_0000_0001);
        }

        #[test]
        fn one_rightmost_one_trailing_zero()
        {
            use super::one_rightmost_one_trailing_zero as orotz;
            assert_eq!(orotz(0b_0000_0000), 0b_1111_1111);
            assert_eq!(orotz(0b_0000_0001), 0b_0000_0001);
            assert_eq!(orotz(0b_0000_0011), 0b_0000_0001);
            assert_eq!(orotz(0b_0001_0011), 0b_0000_0001);
            assert_eq!(orotz(0b_0000_0010), 0b_0000_0011);
            assert_eq!(orotz(0b_0101_1000), 0b_0000_1111);
            assert_eq!(orotz(0b_0111_1111), 0b_0000_0001);
            assert_eq!(orotz(0b_1000_0000), 0b_1111_1111);
            assert_eq!(orotz(0b_1110_1111), 0b_0000_0001);
            assert_eq!(orotz(0b_1111_1110), 0b_0000_0011);
            assert_eq!(orotz(0b_1111_1111), 0b_0000_0001);
        }

        #[test]
        fn one_rightmost_zero_trailing_one()
        {
            use super::one_rightmost_zero_trailing_one as orotz;
            assert_eq!(orotz(0b_0000_0000), 0b_0000_0001);
            assert_eq!(orotz(0b_0000_0001), 0b_0000_0011);
            assert_eq!(orotz(0b_0000_0011), 0b_0000_0111);
            assert_eq!(orotz(0b_0001_0011), 0b_0000_0111);
            assert_eq!(orotz(0b_0000_0010), 0b_0000_0001);
            assert_eq!(orotz(0b_0101_1000), 0b_0000_0001);
            assert_eq!(orotz(0b_0111_1111), 0b_1111_1111);
            assert_eq!(orotz(0b_1000_0000), 0b_0000_0001);
            assert_eq!(orotz(0b_1110_1111), 0b_0001_1111);
            assert_eq!(orotz(0b_1111_1110), 0b_0000_0001);
            assert_eq!(orotz(0b_1111_1111), 0b_1111_1111);
        }

        #[test]
        fn unset_rightmost_contiguous_ones()
        {
            let chk = |contig: fn(u8) -> u8| {
                assert_eq!(contig(0b_0000_0000), 0b_0000_0000);
                assert_eq!(contig(0b_0000_0001), 0b_0000_0000);
                assert_eq!(contig(0b_0000_0011), 0b_0000_0000);
                assert_eq!(contig(0b_0001_0011), 0b_0001_0000);
                assert_eq!(contig(0b_0000_0010), 0b_0000_0000);
                assert_eq!(contig(0b_0101_1000), 0b_0100_0000);
                assert_eq!(contig(0b_0111_1111), 0b_0000_0000);
                assert_eq!(contig(0b_1000_0000), 0b_0000_0000);
                assert_eq!(contig(0b_1110_1111), 0b_1110_0000);
                assert_eq!(contig(0b_1111_1110), 0b_0000_0000);
                assert_eq!(contig(0b_1100_1100), 0b_1100_0000);
                assert_eq!(contig(0b_1111_1111), 0b_0000_0000);
            };
            chk(super::unset_rightmost_contiguous_ones_1);
            chk(super::unset_rightmost_contiguous_ones_2);
        }
    }
}

pub mod ch2_4_absolute_value
{
    pub fn abs_1(x: i8) -> i8
    {
        let y = x >> 7;
        (x ^ y).wrapping_sub(y)
    }

    pub fn abs_2(x: i8) -> i8
    {
        let y = x >> 7;
        x.wrapping_add(y) ^ y
    }

    pub fn abs_3(x: i8) -> i8
    {
        let y = x >> 7;
        x.wrapping_sub(x << 1 & y)
    }

    pub fn nabs_1(x: i8) -> i8
    {
        let y = x >> 7;
        y.wrapping_sub(x ^ y)
    }

    pub fn nabs_2(x: i8) -> i8
    {
        let y = x >> 7;
        y.wrapping_sub(x) ^ y
    }

    pub fn nabs_3(x: i8) -> i8
    {
        let y = x >> 7;
        (x << 1 & y).wrapping_sub(x)
    }

    #[cfg(test)]
    mod validate
    {
        #[test]
        fn abs()
        {
            let chk = |abs: fn(i8) -> i8| {
                assert_eq!(abs(-128), -128);
                assert_eq!(abs(-127), 127);
                assert_eq!(abs(-1), 1);
                assert_eq!(abs(0), 0);
                assert_eq!(abs(1), 1);
                assert_eq!(abs(127), 127);
            };
            chk(super::abs_1);
            chk(super::abs_2);
            chk(super::abs_3);
        }

        #[test]
        fn nabs()
        {
            let chk = |nabs: fn(i8) -> i8| {
                assert_eq!(nabs(-128), -128);
                assert_eq!(nabs(-127), -127);
                assert_eq!(nabs(-1), -1);
                assert_eq!(nabs(0), 0);
                assert_eq!(nabs(1), -1);
                assert_eq!(nabs(127), -127);
            };
            chk(super::nabs_1);
            chk(super::nabs_2);
            chk(super::nabs_3);
        }
    }
}

pub mod ch2_5_average
{
    /** Compute average |_ (x+y)/2 _| without overflow. */
    pub fn floor_avg(x: u8, y: u8) -> u8 { (x & y).wrapping_add((x ^ y) >> 1) }

    /** Compute average |¯ (x+y)/2 ¯| without overflow. */
    pub fn ceil_avg(x: u8, y: u8) -> u8 { (x | y).wrapping_sub((x ^ y) >> 1) }

    #[cfg(test)]
    mod validate
    {
        #[test]
        fn floor_avg()
        {
            pub use super::floor_avg as avg;
            assert_eq!(avg(0, 0), 0);
            assert_eq!(avg(0, 1), 0);
            assert_eq!(avg(0, 2), 1);
            assert_eq!(avg(0, 255), 127);
            assert_eq!(avg(1, 0), 0);
            assert_eq!(avg(1, 1), 1);
            assert_eq!(avg(2, 1), 1);
            assert_eq!(avg(127, 127), 127);
            assert_eq!(avg(254, 255), 254);
            assert_eq!(avg(255, 0), 127);
            assert_eq!(avg(255, 254), 254);
            assert_eq!(avg(255, 255), 255);
        }

        #[test]
        fn ceil_avg()
        {
            pub use super::ceil_avg as avg;
            assert_eq!(avg(0, 0), 0);
            assert_eq!(avg(0, 1), 1);
            assert_eq!(avg(0, 2), 1);
            assert_eq!(avg(0, 255), 128);
            assert_eq!(avg(1, 0), 1);
            assert_eq!(avg(1, 1), 1);
            assert_eq!(avg(2, 1), 2);
            assert_eq!(avg(127, 127), 127);
            assert_eq!(avg(254, 255), 255);
            assert_eq!(avg(255, 0), 128);
            assert_eq!(avg(255, 254), 255);
            assert_eq!(avg(255, 255), 255);
        }
    }
}

pub mod ch2_6_sign_extend
{
    pub fn extend_1(x: u16) -> u16
    {
        (x.wrapping_add(0x0080) & 0x00ff).wrapping_sub(0x0080)
    }

    pub fn extend_2(x: u16) -> u16
    {
        ((x & 0x00ff) ^ 0x0080).wrapping_sub(0x0080)
    }

    pub fn extend_3(x: u16) -> u16 { (x & 0x007f).wrapping_sub(x & 0x80) }

    #[cfg(test)]
    mod validate
    {
        #[test]
        fn extend()
        {
            let chk = |ext: fn(u16) -> u16| {
                assert_eq!(ext(0b0000_0000_0000_0000), 0b0000_0000_0000_0000);
                assert_eq!(ext(0b0000_0000_0000_0001), 0b0000_0000_0000_0001);
                assert_eq!(ext(0b0000_0000_0100_0001), 0b0000_0000_0100_0001);
                assert_eq!(ext(0b0000_0000_0111_1111), 0b0000_0000_0111_1111);
                assert_eq!(ext(0b0000_0000_1000_0000), 0b1111_1111_1000_0000);
                assert_eq!(ext(0b0000_0000_1000_0001), 0b1111_1111_1000_0001);
                assert_eq!(ext(0b0000_0000_1111_1111), 0b1111_1111_1111_1111);
                assert_eq!(ext(0b1111_1111_1000_0001), 0b1111_1111_1000_0001);
                assert_eq!(ext(0b1111_1111_1111_1111), 0b1111_1111_1111_1111);
            };
            chk(super::extend_1);
            chk(super::extend_2);
            chk(super::extend_3);
        }
    }
}

pub mod ch2_7_compare
{
    #[derive(Debug, PartialEq)]
    pub enum Sign
    {
        Less    = -1,
        Zero    = 0,
        Greater = 1,
    }

    impl TryFrom<i8> for Sign
    {
        type Error = ();

        fn try_from(x: i8) -> Result<Self, Self::Error>
        {
            match x {
                -1 => Ok(Self::Less),
                0 => Ok(Self::Zero),
                1 => Ok(Self::Greater),
                _ => Err(()),
            }
        }
    }

    pub fn sign_1(x: i8) -> Sign
    {
        if x < 0 {
            Sign::Less
        } else if x == 0 {
            Sign::Zero
        } else {
            Sign::Greater
        }
    }

    pub fn sign_2(x: i8) -> Sign
    {
        Sign::try_from((x >> 7) | (0u8.wrapping_sub(x as u8) >> 7) as i8)
            .unwrap()
    }

    pub fn sign_3(x: i8) -> Sign
    {
        Sign::try_from(((x > 0) as i8).wrapping_sub((x < 0) as i8)).unwrap()
    }

    pub fn sign_4(x: i8) -> Sign
    {
        Sign::try_from(((x >= 0) as i8).wrapping_sub((x <= 0) as i8)).unwrap()
    }

    #[cfg(test)]
    mod validate
    {
        use super::Sign;

        #[test]
        fn sign()
        {
            let chk = |sign: fn(i8) -> Sign| {
                assert_eq!(sign(-128), Sign::Less);
                assert_eq!(sign(-42), Sign::Less);
                assert_eq!(sign(-1), Sign::Less);
                assert_eq!(sign(0), Sign::Zero);
                assert_eq!(sign(1), Sign::Greater);
                assert_eq!(sign(42), Sign::Greater);
                assert_eq!(sign(127), Sign::Greater);
            };
            chk(super::sign_1);
            chk(super::sign_2);
            chk(super::sign_3);
            chk(super::sign_4);
        }
    }
}
